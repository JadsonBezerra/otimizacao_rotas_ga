import random

TAXA_MUTACAO = 0.01
TAXA_CRUZAMENTO = 0.9
TAMANHO_POPULACAO = 20
NUMERO_GERACOES = 1


class no:
    def __init__(self,x,y,vizinhos=[]):
        self.x=x
        self.y=y
        self.vizinhos=vizinhos

    def addVizinho(self,vizinho):
        self.vizinhos=self.vizinhos+[vizinho]
        vizinho.vizinhos=vizinho.vizinhos+[self]


    def vizinhoAleatorio(self):
        return self.vizinhos[random.randint(0,len(self.vizinhos)-1)]

    def aindaTemVizinhosForaDessaLista(self,lista):
        for vizinho in self.vizinhos:
            if not lista.__contains__(vizinho):
                return True
        return False
    def __repr__(self):
        return '(%i,%i)' % (self.x,self.y)

grafo = [no(1,0),no(2,0),no(3,1),no(3,3),no(2,5),no(1,5),no(0,4),no(0,2),no(1,2)]
grafo[0].addVizinho(grafo[8])
grafo[0].addVizinho(grafo[1])
grafo[1].addVizinho(grafo[2])
grafo[1].addVizinho(grafo[4])
grafo[2].addVizinho(grafo[3])
grafo[3].addVizinho(grafo[4])
grafo[4].addVizinho(grafo[5])
grafo[5].addVizinho(grafo[6])
grafo[6].addVizinho(grafo[7])
grafo[7].addVizinho(grafo[8])
grafo[6].addVizinho(grafo[8])

# print(grafo)

INICIO = grafo[5]
FINAL = grafo[0]

# print(grafo[-3])


class cromossomo:
    def __init__(self,caminho=[]):
        self.caminho = caminho if caminho != [] else self.criarAletorio(INICIO,FINAL)

    def criarAletorio(self,inicio,final):
        if inicio == final:
            return [inicio]
        noAtual=inicio
        caminhoAleatorio=[noAtual]
        while noAtual!=final and noAtual.aindaTemVizinhosForaDessaLista(caminhoAleatorio):
            proximoNo=noAtual.vizinhoAleatorio()
            while caminhoAleatorio.__contains__(proximoNo):
                proximoNo=noAtual.vizinhoAleatorio()
            noAtual=proximoNo
            caminhoAleatorio=caminhoAleatorio+ [noAtual]
        if caminhoAleatorio[-1] != final:
            return self.criarAletorio(inicio,final)
        return caminhoAleatorio

    def mutacao(self):
        inicial=random.randint(0,len(self.caminho)-1)
        final=random.randint(inicial,len(self.caminho)-1)
        if inicial != final:
            novoCaminho=self.criarAletorio(self.caminho[inicial],self.caminho[final])
            self.caminho=self.caminho[:inicial] + novoCaminho + self.caminho[final+1:]

    def cruzamento(self,parceiro):
        inter = [x for x in self.caminho if x in parceiro.caminho]
        noEscolhido= inter[random.randint(0,len(inter)-1)]
        if noEscolhido :
            filhoUm = cromossomo(self.caminho[:self.caminho.index(noEscolhido)]+parceiro.caminho[parceiro.caminho.index(noEscolhido):])
            filhoDois = cromossomo(parceiro.caminho[:parceiro.caminho.index(noEscolhido)]+self.caminho[self.caminho.index(noEscolhido):])
        return filhoUm,filhoDois

    def fitness(self):
        custo = 0
        for i in range(0,len(self.caminho)-1):
            custo += ((self.caminho[i].x-self.caminho[i+1].x)**2+(self.caminho[i].y-self.caminho[i+1].y)**2)**0.5       
        return custo

    def __repr__(self):
        return self.caminho.__repr__()


def torneio(populacao):
    print(populacao)
    novaPopulacao = []
    for i in range(len(populacao)):
        candidatoUm = random.randint(0,len(populacao)-1)    
        candidatoDois = random.randint(0,len(populacao)-1)    
        candidatoTres = random.randint(0,len(populacao)-1)
        print('candidadoum', candidatoUm)
        print('candidadodos', candidatoDois)
        print('candidadotres', candidatoTres)
        disputa = [populacao[candidatoUm],populacao[candidatoDois],populacao[candidatoTres]]
        print('antes da disputa', disputa)
        disputa = sorted(populacao, key=lambda cromossomo: cromossomo.fitness())
        print('pos disp', disputa)
        print('\n')
        novaPopulacao.append(disputa[0])

    return novaPopulacao

def elitismo(populacao,quantidadeDesejada):
    return sorted(populacao, key=lambda cromossomo: cromossomo.fitness())[0:quantidadeDesejada-1]


def coeficienteDeVariancia(populacao):
    media = 0
    tamanho=len(populacao)
    for individuo in populacao:
        # print(individuo.caminho)
        media += individuo.fitness()
    media/=tamanho
    desvioPadrao = 0
    for individuo in populacao:
        desvioPadrao += (media-individuo.fitness())**2
    desvioPadrao /= (tamanho-1)
    desvioPadrao = (desvioPadrao)**0.5
    cv = desvioPadrao/abs(media)

    return cv


populacao = [cromossomo() for i in range(0,TAMANHO_POPULACAO)]

geracao = 0

while geracao < NUMERO_GERACOES and coeficienteDeVariancia(populacao) > 0.001:
    geracao += 1
    populacao = torneio(populacao)
    print(populacao)
    novaGeracao = []
    for  i in range(TAMANHO_POPULACAO):
        if random.random() < TAXA_CRUZAMENTO:
            pai = random.randint(0, len(populacao)-1)
            mae = random.randint(0, len(populacao)-1)
            # print('pai',pai,populacao[pai])
            # print(mae,populacao[mae])
            while mae == pai:
                mae = random.randint(0, len(populacao)-1)
            filhoUm,filhoDois=populacao[pai].cruzamento(populacao[mae])
            novaGeracao.append(filhoUm)
            novaGeracao.append(filhoDois)
    
    for novoIndividuo in novaGeracao:
        if random.random() < TAXA_MUTACAO:
            novoIndividuo.mutacao()

    populacao = elitismo(populacao+novaGeracao,TAMANHO_POPULACAO)
